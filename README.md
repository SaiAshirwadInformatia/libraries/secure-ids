# Secure Ids

[![Latest Version on Packagist](https://img.shields.io/packagist/v/saiashirwadinformatia/secure-ids.svg?style=flat-square)](https://packagist.org/packages/saiashirwadinformatia/secure-ids)
<!-- [![GitHub Tests Action Status](https://img.shields.io/github/workflow/status/saiashirwadinformatia/secure-ids/run-tests?label=tests)](https://github.com/saiashirwadinformatia/secure-ids/actions?query=workflow%3Arun-tests+branch%3Amain) -->
<!-- [![GitHub Code Style Action Status](https://img.shields.io/github/workflow/status/saiashirwadinformatia/secure-ids/Check%20&%20fix%20styling?label=code%20style)](https://github.com/saiashirwadinformatia/secure-ids/actions?query=workflow%3A"Check+%26+fix+styling"+branch%3Amain) -->
[![Total Downloads](https://img.shields.io/packagist/dt/saiashirwadinformatia/secure-ids.svg?style=flat-square)](https://packagist.org/packages/saiashirwadinformatia/secure-ids)

---
## Installation

You can install the package via composer:

```bash
composer require saiashirwadinformatia/secure-ids
```

You can publish the config file with

```bash
php artisan vendor:publish --provider="SaiAshirwadInformatia\SecureIds\SecureIdsServiceProvider"
```

This is the contents of the published config file:

```php
return [
    'models_directory' => 'Models',
    'model_namespace'  => 'App\\Models\\',
    'length'           => 13,
];
```

## Usage

Load the configs of SecureIds by calling in your AppServiceProvider `boot` method

```php
use SaiAshirwadInformatia\SecureIds\Facades\SecureIds;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        SecureIds::load();
    }

```

Use trait `HasSecureIds` for the models you want to enable securing the id.

```php
use SaiAshirwadInformatia\SecureIds\Models\Traits\HasSecureIds;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    // Add below Trait to enable securing id's
    use HasSecureIds;
    // ...
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Credits

- [Rohan Sakhale](https://gitlab.com/rsakhale)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
