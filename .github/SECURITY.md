# Security Policy

If you discover any security related issues, please email rs@saiashirwad.com instead of using the issue tracker.
