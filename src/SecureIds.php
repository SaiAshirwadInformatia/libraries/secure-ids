<?php

namespace SaiAshirwadInformatia\SecureIds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Vinkla\Hashids\Facades\Hashids;

class SecureIds
{
    private int $length = 13;

    public function __construct()
    {
        $this->load();
    }

    public function encode(string $className, int $value): string
    {
        return Hashids::connection($className)->encode($value);
    }

    public function decode(string $className, string|int $hash): int|string|bool
    {
        if (is_numeric($hash)) {
            return $hash;
        }

        if (!$hash) {
            return false;
        }

        $decode = Hashids::connection($className)->decode($hash);
        if (isset($decode[0])) {
            return $decode[0];
        }
        return false;
    }

    public function load(): void
    {
        $models_directory = app_path(config('secure_ids.models_directory'));
        $models_namespace = config('secure_ids.model_namespace');
        $this->length = config('secure_ids.length');
        $hashIdsConfigs = config('hashids');

        $hashIdsConfigs = $this->registerModelBindings(
            $hashIdsConfigs, $models_directory, $models_namespace
        );
        config(['hashids' => $hashIdsConfigs]);

        $this->registerRouteBinding($models_directory, $models_namespace);
    }

    protected function registerModelBindings(array $config, string $path, string $namespace): array
    {
        $classes = array_slice(scandir($path), 2);

        foreach ($classes as $classPath) {
            /*
             * If class path is identified as directory
             * This is possibility a sub-namespace
             *
             * Register Model Bindings for this directory/namespace
             */
            if (is_dir($path . DIRECTORY_SEPARATOR . $classPath)) {
                $newScanPath = $path . DIRECTORY_SEPARATOR . $classPath;
                $newNamespace = $namespace . basename($classPath, ".php") . '\\';
                $config = $this->registerModelBindings($config, $newScanPath, $newNamespace);
                continue;
            }

            /*
             * Verify the class path identified is a php file
             */
            if (!Str::endsWith($classPath, '.php')) {
                continue;
            }

            /*
             * Load only the class name removing the complete path
             */
            $className = $namespace . basename($classPath, '.php');

            if (!class_exists($className)) {
                continue;
            }
            $salt = $className . md5($className) . config('app.key');
            $config['connections'][$className] = [
                'salt' => $salt,
                'length' => $this->length,
            ];
        }

        return $config;
    }

    /**
     * This function works in recursive way to register directories within Models
     * and identifies the classes to load their
     */
    private function registerRouteBinding(string $path, string $namespace): void
    {
        $classes = array_slice(scandir($path), 2);

        foreach ($classes as $idx => $classPath) {

            if (is_dir($path . DIRECTORY_SEPARATOR . $classPath)) {
                $newScanPath = $path . DIRECTORY_SEPARATOR . $classPath;
                $newNamespace = $namespace . basename($classPath, ".php") . '\\';
                $this->registerRouteBinding($newScanPath, $newNamespace);
                continue;
            }
            if (!Str::endsWith($classPath, '.php')) {
                continue;
            }

            $key = Str::snake(basename($classPath, '.php'));

            $className = $namespace . basename($classPath, '.php');
            if (!class_exists($className)) {
                continue;
            }
            try {
                Route::bind($key, function ($value, $route = null) use ($className, $key) {
                    return $this->getModel($className, $value);
                });
            } catch (\Exception $e) {
                // Do nothing
            }
        }
    }

    private function getModel(string $model, string $routeKey): ?Model
    {
        if (!is_numeric($routeKey)) {
            $routeKey = $model::decodeKey($routeKey) ?? null;
        }
        $modelInstance = resolve($model);

        if (method_exists($modelInstance, 'withTrashed')) {
            return $modelInstance->withTrashed()->findOrFail($routeKey);
        } else {
            return $modelInstance->findOrFail($routeKey);
        }
    }
}
