<?php

namespace SaiAshirwadInformatia\SecureIds;

use Illuminate\Support\ServiceProvider;

class SecureIdsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/secure_ids.php' => config_path('secure_ids.php'),
        ]);

        $this->app->singleton(SecureIds::class, function ($app) {
            return new SecureIds;
        });
    }

    public function register()
    {

    }
}
