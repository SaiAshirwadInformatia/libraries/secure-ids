<?php

namespace SaiAshirwadInformatia\SecureIds\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SaiAshirwadInformatia\SecureIds\SecureIds;
use Vinkla\Hashids\Facades\Hashids;

trait HasSecureIds
{
    /**
     * @var array
     */
    protected array $fieldClassMapper = [

    ];

    public function scopeEncodeKey(Builder|\Illuminate\Database\Query\Builder $query, $key): ?string
    {
        if (!$key) {
            return null;
        }
        return Hashids::connection(self::class)->encode($key);
    }

    public function scopeFindByKey(Builder|\Illuminate\Database\Query\Builder $query, string|int $hash): ?Model
    {
        if (is_numeric($hash)) {
            /*
             * If this is numeric it means it's an `id` based classifier
             */
            $id = $hash;
            return self::find($id);
        }
        $id = $this->scopeDecodeKey($query, $hash);

        if ($id && is_numeric($id)) {
            return $this->find($id);
        }
        return null;
    }

    public function scopeFindByKeyOrFail(Builder|\Illuminate\Database\Query\Builder $query, string|int $hash): ?Model
    {
        if (is_numeric($hash)) {
            $id = $hash;
            return $this->findOrFail($id);
        }

        $id = $this->scopeDecodeKey($query, $hash);
        if ($id) {
            return $this->findOrFail($id);
        }
        abort(404);
    }

    public function scopeDecodeKey(Builder|\Illuminate\Database\Query\Builder $query, string|int $hash): int|string|bool
    {
        return SecureIds::decode(self::class, $hash);
    }

    public function getSecureIdAttribute(): string
    {
        return $this->getRouteKey();
    }

    public function getRouteKey(): string
    {
        return $this->encodeKey($this->getKey());
    }

    public function toClass(): string
    {
        return $this->getTable();
    }

    public function toArray(): array
    {
        $arr = parent::toArray();
        if ($this->id) {
            if (!request()->is('api/*')) {
                $arr['secure_id'] = $this->secure_id;
            } else {
                $arr['id'] = $this->secure_id;
            }
        }

        // Timezone
        foreach ($arr as $key => $value) {
            if (!$value) {
                continue;
            } else if (Str::endsWith($key, '_id')) {
                $objectClass = Str::replaceFirst('_id', '', $key);
                $typeClass = $objectClass . '_type';
                $className = Str::title($objectClass);
                $namespace = config("secureids.model_namespace");
                $objectClass = $namespace . $className;

                if (isset($this->fieldClassMapper[$key])) {
                    $objectClass = $this->fieldClassMapper[$key];
                } else if ($this->$typeClass) {
                    $objectClass = Relation::getMorphedModel($this->$typeClass);
                } else if ('parent_id' == $key) {
                    $objectClass = get_called_class();
                }

                if (!class_exists($objectClass)) {
                    Log::warning("[SecureIds] Class not found: " . $objectClass . " original key: " . $key);
                    continue;
                }
                $arr[$key] = $objectClass::encodeKey($value);
            }
        }
        return $arr;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value): void
    {
        if (Str::endsWith($key, '_id') && !is_numeric($value)) {
            $objectClass = Str::replaceFirst('_id', '', $key);
            $typeClass = $objectClass . '_type';

            $className = Str::title($objectClass);

            $namespace = config("secureids.model_namespace");
            $objectClass = $namespace . $className;

            if (isset($this->fieldClassMapper[$key])) {
                $objectClass = $this->fieldClassMapper[$key];
            } else if ($this->$typeClass) {
                $objectClass = Relation::getMorphedModel($this->$typeClass);
            } else if ('parent_id' == $key) {
                $objectClass = get_called_class();
            }

            if (!class_exists($objectClass)) {
                Log::warning("[SecureIds] Class not found: " . $objectClass . " original key: " . $key);
            } else {
                $value = $objectClass::decodeKey($value);
            }
        }
        parent::setAttribute($key, $value);
    }

}
