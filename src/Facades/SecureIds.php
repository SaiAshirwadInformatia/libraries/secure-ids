<?php

namespace SaiAshirwadInformatia\SecureIds\Facades;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;
use SaiAshirwadInformatia\SecureIds\SecureIds as SecureIdsService;

/**
 * @see \SaiAshirwadInformatia\SecureIds\SecureIds
 */
class SecureIds extends Facade
{
    protected static function getFacadeAccessor()
    {
        return resolve(SecureIdsService::class);
    }
}
